# Session 2 - Linguistique des discours, traitement automatique des langues.

Séance du 14 février 2022, 10h – 12h  
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 (1er étage) et en ligne  


## Programme

1. Présentation de la thématique 2 : Linguistique des discours, traitement automatique des langues. 
2. Communications présentées :

### Classification d’articles encyclopédiques et reconnaissance d’entités nommées : Application à l’Encyclopédie de Diderot et d’Alembert

L. Moncla (INSA/LIRIS), D. Vigier (U. Lyon 2/ICAR), K. McDonough (A. Turing Institute), A. Brenon (INSA/LIRIS & ICAR), T. Joliveau (U. St-Etienne/EVS)

Cette présentation s’intéressera à deux tâches en cours au sein du Projet GEODE et dont certains résultats préliminaires ont déjà été publiés. La première tâche concerne la problématique de classification automatique des articles encyclopédiques. Nous présenterons une étude comparative de différentes approches de classification supervisée (associées à différentes méthodes de vectorisation des textes) telles que des méthodes d’apprentissage classiques (*Logistic Regression*, *SGD*, *SVM*), des méthodes d’apprentissage profond (*CNN* et *LSTM*) et des architectures à base de *Transformers* et de modèles de langue pré-entrainés (BERT et CamemBERT). La deuxième tâche s’intéresse à la reconnaissance et à la classification des entités nommées. Nous présenterons une méthodologie de recherche d’indices linguistiques utilisant la plateforme TXM pour leur découverte et la plateforme PERDIDO pour leur implémentation. Enfin, nous présenterons les premières évaluations de notre approche.

[Supports de présentation](./Slides_presentation_01_GEODE.pdf)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session2_fev22/seminaire-session2-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session2_fev22/seminaire-session2-presentation1.mp4 "Enregistrement présentation 1")

### Analyse et enrichissement sémantique de documents numérisés : le cas d'usage de la presse ancienne Européenne au sein du projet NewsEye

Antoine Doucet (U. La Rochelle /L3i)

De nombreux documents ne peuvent être rendus accessibles à une analyse automatique que sous forme d'images numérisées. C'est notamment le cas de tout document historique ou manuscrit, mais aussi celui de nombreux documents numériques natifs, passés sous forme d'image pour diverses raisons (par exemple : conversion de fichier ou passage par l'analogue pour insérer une signature manuscrite, faire un envoi postal, etc.).
Pouvoir analyser le contenu textuel de tels documents numérisés requière une phase de conversion depuis l'image capturée vers une représentation textuelle, dont une partie clé est la reconnaissance optique de caractères (OCR). Le texte qui en résulte est souvent imparfait, dans une mesure qui est notamment corrélée à la qualité du support initial (qui peut-être tâché, plié, vieilli, etc.) et à la qualité de l'image qui en a été prise.
Cette communication présentera de récentes avancées en IA et traitement automatique des langues permettant d'analyser ce type de corpus de façon robuste aux erreurs d'OCR. Je montrerai par exemple comment nous avons pu dans le cadre du projet NewsEye créer l'état de l'art en reconnaissance et désambiguïsation cross-lingue des entités nommées (noms de lieux, mais aussi de personnes, et d'organisations) dans des corpus de presse ancienne rédigés en 4 langues entre 1850 et 1950, et ce malgré des corpus particulièrement dégradés. Ce type de résultat ouvre la voie à une analyse à grande échelle, qui peut notamment s'affranchir des frontières (linguistiques).


[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session2_fev22/seminaire-session2-presentation2.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session1_jan22/seminaire-session2-presentation2.mp4 "Enregistrement présentation 2")