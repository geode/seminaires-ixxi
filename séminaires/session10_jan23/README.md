# Session 10 - Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.


Séance du 24 janvier 2023, 14h – 15h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne


## Programme


### La place de la carte au XVIIIe siècle

Nicolas Verdier (CNRS, EHESS)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session10_jan23/seminaire-session10-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session10_jan23/seminaire-session10-presentation1.mp4 "Enregistrement présentation")

[Supports de présentation](./Slides_presentation_Verdier.pdf)

Dans cette intervention on s’intéressera à deux aspects de la place des cartes au XVIIIe siècle. Le premier portera sur la question de la relation entre carte et géographie au travers de la production de livres contenant des cartes durant un long XVIIIe siècle. Le deuxième portera sur les formes de la présence des cartes dans les livres en soulevant les cas des livres à cartes, et des livres de cartes, entendons les atlas.

#### Bio

Nicolas Verdier est directeur de recherche au CNRS et directeur d’étude cumulant à l’EHESS avec une chaire de géohistoire. Ses travaux portent sur l’histoire des rapports à l’espace et aux territoires sur un temps long qui courre de la fin du XVIIe au XXIe siècles. Il a plus particulièrement travaillé l’histoire des réseaux de transport et l’histoire de la cartographie. Ses recherches actuelles portent sur l’histoire des circonscriptions et l’histoire de la vitesse.
 
 
#### Publications sur l’histoire de la cartographie :
 
* Verdier N., 2022, « Quelques réflexions sur la matérialité des atlas » in Besse J.-M. (dir), Forme de savoir, forme de pouvoir. Les atlas géographiques à l’époque moderne et contemporaine, Rome, Éditions de l’Ecole Française de Rome, pp. 301-320.
* Quatre textes avec Jean-Marc Besse dans : Matthew H. Edney et Mary Sponberg Pedley (Ed.), (2020) History of Cartography. XVIIIe century, Chicago University Press : « Art and Design of maps » (pp. 117-125) ; “Color and cartography » (pp. 294-302) ; « Cartouche » (pp. 244-251) ; « Iconography », (pp. 651-658). 
* Verdier N., 2019, “Courte histoire d’un échec : le mariage de l’armée et du cadastre dans le premier quart du XIXe siècle », La Revue Cartes & Géomatiques, n°236, pp. 11-24.
* Verdier N., 2018, « Rendre présent le futur : les enjeux des représentations planimétriques au XVIIIe siècle », in Graber F. et Gireaudeau M.-L, Les projets comme institutions XVIIIe-XXe siècles, Paris, Presses des Mines, pp. 149-162.
* Verdier N., 2018, « Entre publicité, débat scientifique et vulgarisation : Jean-Baptiste d’Anville et les journaux savants », in Hofmann Catherine et Haguet Lucile (dir.), Jean-Baptiste d’Anville, un cabinet savant au siècle des Lumières, Oxford/Paris, Oxford University Studies in the Enlightenment/BNF, pp. 237-260.
* Verdier N., 2016, « Aux limites de la figuration cartographique Le paysage comme lieu de la séparation entre vue et carte », Geostorie, Bolletino e Notiziario del Centro Italiano per gli Studi Storci-Geografici, XXIV, n°1, pp. 24-41. 
http://www.cisge.it/blog/geostorie/1-2-2016/
* Verdier N., 2016, « Les premiers atlas nationaux », Clio en Cartes 3 – La carte fait-elle le territoire ?, Publication en ligne (Atlas historique d’Alsace).
* Verdier N., 2016, « La cartographie et ses matérialités induites : les objets intermédiaires de la carte », in Besse J.-M. et Tibergien G. A., Opérations cartographiques, Actes Sud/ENSP, Marseille/Versailles, pp. 156-167.
* Verdier N., 2016, « Estate maps in 18th century France : between representation of land rights the production of accurate maps », in Montanari S. (dir.), Cartography and Cadastral Maps, Visions from the Past for a vision of our future, Pise, Edizioni della Normale, (en ligne), pp. 31-39. 
http://edizioni.sns.it/it/downloadable/download/sample/sample_id/129/
* Verdier N., 2015, « Des cartes en situation d’incertitude : la controverse sur le Kamtchatka entre 1737 et 1738 comme révélateur d’une crise de la cartographie française », Rytchalovsky E. (dir.), География эпохи Просвещения : между воображением и реальностью , Moscou, Naouca, pp. 100-120.
* Verdier N.,2015, La carte avant les cartographes, l’avènement du régime cartographique en France au XVIIIe siècle, Presses de la Sorbonne, Paris.
* Verdier N., 2015, "Entre diffusion de la carte et affirmation des savoirs géographiques en France : les paradoxes de la mise en place de la carte géographique au XVIIIe siècle", L'espace géographique, n°1, pp. 38-56.
* Bezes B. Verdier N., et Robert S., 2015, « La représentation des routes sur les cartes anciennes », in Robert S et Verdier N. (dir.) 2015, Dynamique et résiliences des réseaux routiers en Ile de France, 52e Supplément à la Revue Archéologique du Centre de la France, pp. 95-112.
* Verdier N., 2011, « Les formes du voyage : cartes et espaces des guides de voyage », In Situ, Revue des Patrimoines [en ligne], n° 15 (18 p.). https://journals.openedition.org/insitu/573


