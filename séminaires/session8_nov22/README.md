# Session 8 - Systèmes d’information géographique, Géomatique et visualisation des données.

Séance du 24 novembre 2022, 14h – 16h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne


## Programme



### Labels and Symbols: the representation of Antiquities on the Ordnance Survey maps of Great Britain, and their digital annotation.

Valeria Vitale (University of Sheffield)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session8_nov22/seminaire-session8-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session8_nov22/seminaire-session8-presentation1.mp4 "Enregistrement présentation 1")

The Ordnance Survey (OS) is Great Britain’s national mapping agency. It was born in 1745, and its maps have played an important role in shaping the nation. This rich corpus of historical documents has been digitised, and it is today in large part available to the public.  Not only these maps are valuable means to investigate past representations and perceptions of space, but they also show an uncommon feature that makes them especially interesting to archaeologists, historians and heritage specialists: OS maps record the location of antiquities, both extant and disappeared. This practice implies that, unlike many others, OS maps are committed to represent not only the visible features of the landscape, but even the invisible ones, blending different temporal layers in a single, complex document.
To deliver many layers of information, OS maps combine different codes, making the most of polysemic signs. In particular, a separate system to represent antiquities and historical sites relies on the combination of ad hoc labels, dedicated vocabulary, and special fonts. 
The information about antiquities that can be found on OS maps is not only interesting from a semiotics perspective, for its elegant use of intersecting codes to deliver a large amount of information, but it is also a valuable historical resource that enables research questions around the representation of Scottish heritage, the influence of British Imperialism in the reception of cultural heritage, and the evolution of archaeological practices. These special labels also offer rare diachronic insights on the mapping process itself, and a new understanding of the complex provenance of the information recorded in historical maps.
Machines Reading Maps is an international collaboration between the Alan Turing Institute, the University of Minnesota, the Austrian Institute of Technology, and the University of Southern California. The project uses a combination of machine-learning and semantic technologies to detect and collect text on digitised maps, and to link it with external knowledge bases such as WikiData or OpenStreetMap. We want to apply this novel approach to the understanding of Victorian reception of antiquities, using the OS maps as a mirror of contemporary ideas and sensibilities. In the seminar, we plan to discuss how the process of digital annotation helped us to translate humanities research questions into machine-learning tasks that will enable us to process a large number of maps, creating new datasets and, ideally, delivering new answers.



### Machines Reading Maps and Recogito: an Online Environment for the Annotation of Historic Maps

Rainer Simon (Austrian Institute of Technology)

[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session8_nov22/seminaire-session8-presentation2.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session8_nov22/seminaire-session8-presentation2.mp4 "Enregistrement présentation 2")


Maps constitute a significant body of global cultural heritage, and the number of maps available digitally is increasing rapidly. The Machines Reading Maps project from The Alan Turing Institute, University of Minnesota, Austrian Institute of Technology, University of Sheffield, and the University of Southern California explores how Machine Learning can be used to extract text from maps, making cartographic collections more accessible and useful.

This presentation provides an introduction to the technical foundations of Machines Reading Maps, focusing primarily on Recogito, our annotation environment for generating ground truth and visualizing the results of automatic map text recognition. Recogito is an online environment for collaborative annotation, and can be flexibly customized to meet a wide range of project needs. The presentation will conclude with an outlook on related work that is currently taking place in collaboration with the David Rumsey Map Collection."


