# Session 11 - Linguistique des discours, traitement automatique des langues.


Séance du 20 février 2023, 14h – 16h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne



## Programme


### SoDUCo : croisement de sources géo-historiques pour l'étude de l'évolution de Paris de 1789 à 1950

Julien Perret (IGN) et Joseph Chazalon (Epita)


[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session11_fev23/seminaire-session11-presentation1.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session11_fev23/seminaire-session11-presentation1.mp4 "Enregistrement présentation")

[Supports de présentation](https://docs.google.com/presentation/d/1i2EmhkuCrEZ9p2KvYgRUQUoRCFR68bWbzWWeAzUK1Z0/edit#slide=id.p)

Le projet ANR SoDUCo développe des approches, des outils et des données pour l'étude des transformations spatiales en relation avec l'évolution des activités professionnelles de Paris de 1789 à 1950. Sur la base d'un corpus de sources primaires soigneusement établi, contenant à la fois des cartes (atlas de Verniquet, atlas municipal, etc.) et des annuaires professionnels (annuaires Bottin, Didot, etc.), notre équipe travaille à l'extraction (tantôt massive et automatique, tantôt ciblée et manuelle) de l'énorme masse d'information contenue dans ces documents, mais également au croisement, à la validation et à l'analyse de ces données. Nous nous attachons à suivre une démarche ouverte et reproductible et à rendre nos outils, données et codes utilisables par d'autres équipes. Nous présenterons l'état d'avancement des résultats du projet.



