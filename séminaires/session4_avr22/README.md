# Session 4 - Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.

Séance du **8 avril 2022**, 14h – 16h  
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D 024 (rez-de-chaussée) et en ligne  


## Programme


### Discours géographiques dans les dictionnaires universels des XVIIe-XVIIIe siècles : du DU de Furetière (1690) au DUFLT (1704-1771)

Isabelle Turcan (U. de Lorraine)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session4_avr22/seminaire-session4-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session4_avr22/seminaire-session4-presentation1.mp4 "Enregistrement présentation 1")

La discipline de la géographie se développe en tant que domaine de la connaissance pré-encyclopédique dès la seconde moitié du XVIIe s. si l’on se fie aux nomenclatures enregistrées par Richelet dans son DF, 1680, puis par Furetière dans son DU, 1690, et aux enrichissements de celles des éditions du DUFLT données de 1704 à 1771. En témoignent, outre les définitions de la discipline, les critères métalinguistiques propres aux dictionnaires : les marques de domaine - d’abord hésitantes, voire absentes, puis de plus en plus régulières -, les mots-clés métalinguistiques, du domaine et de ses acteurs, et les sources nommées renvoyant à des traités spécialisés de géographie et de cartographie dont le genre s’est développé considérablement au XVIIIe siècle. 



### Esprit des lumières et cartographie

Henri Desbois (U. de Paris Nanterre)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session4_avr22/seminaire-session4-presentation2.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session4_avr22/seminaire-session4-presentation2.mp4 "Enregistrement présentation 2")

Dans son cinquième Mémoire pour servir à l’histoire des sciences et à celle de l’observatoire royal de Paris (1810), Jean-Dominique Cassini, met en parallèle l’Encyclopédie, la Description des arts et métiers, et la carte de France, comme les trois grandes entreprises intellectuelles du 18ème siècle. Le lien entre les deux premiers ouvrages est bien connu, mais leur rapprochement avec la carte de l’Académie mérite qu’on s’y attarde. 
Je propose d’aborder la cartographie du 18ème siècle non pas tant pour sa signification dans l’histoire de la géographie, mais pour tenter de montrer quelle place elle tient dans l’esprit des Lumières. Je tenterai de montrer notamment comment l’idéal d’objectivité dont Daston et Galison repèrent l’émergence dans les atlas de spécimens du 19ème siècle sous-tend déjà implicitement, et parfois explicitement, la cartographie du 18ème siècle. Les encyclopédistes et les savants des Lumières ont ainsi pu faire de la géographie, entendue comme science de la mesure du monde, un modèle de la connaissance en général. 

