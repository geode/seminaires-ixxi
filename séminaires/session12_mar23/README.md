# Session 12 - Linguistique des discours, traitement automatique des langues.


Séance du 30 mars 2023, 14h – 16h30
INSA Lyon, bâtiment Blaise Pascal (salle 502.3.23) et en ligne


## Programme


### Le rôle des voyages dans la recomposition des géographies européennes : savoirs cartographiques, pratiques de l’espace et descriptions encyclopédiques

Gilles Bertrand (Université Grenoble-Alpes)

[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session12_mar23/seminaire-session12-presentation1.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session12_mar23/seminaire-session12-presentation1.mp4 "Enregistrement présentation")

[Supports de présentation](./Slides_presentation_Bertrand.pdf)

Des savoirs sur les peuples et les gouvernements ainsi que sur les territoires et leurs frontières sont élaborés entre XVIe et XVIIIe siècle par les cosmographies, les entrées d’encyclopédies et les descriptions de guides et itinéraires. Mais un écart se creuse entre ces instruments présents dans les bibliothèques et parfois dotés de cartes et ce que les relations des voyageurs membres des élites révèlent de leurs pratiques effectives : d’un côté persiste un rituel du voyage centré sur les espaces familiers du Grand Tour (Italie, France, Allemagne, Autriche, Suisse, Provinces-Unies, Angleterre) et de l’autre côté, sous l’effet des découvertes extra-européennes et des pratiques diplomatiques, savantes ou marchandes, les tropismes européens en Europe évoluent. La bibliothèque de Voltaire ou le voyage de Diderot en Russie témoignent de tels changements. On se demandera donc dans quelle mesure, surtout au XVIIIe siècle, les élites européennes ont accordé une place croissante à d’autres destinations, le plus souvent simplement imaginées, que celles « classiques » du Grand Tour, se laissant conduire au moins en esprit vers la Scandinavie, la Pologne, la Russie ou le Sud-Est de l’Europe (Balkans, Grèce, Levant).
