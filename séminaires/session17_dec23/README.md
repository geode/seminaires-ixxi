# Session 17 - Linguistique des discours, traitement automatique des langues.

Séance du 4 décembre 2023, 14h – 15h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne


## Programme


### BASNUM: réussites et échecs

Ioanna Galleron (Sorbonne Nouvelle)


[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session17_dec23/seminaire-session17.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session17_dec23/seminaire-session17.mp4 "Enregistrement présentation")


Le projet ANR BASNUM, qui vient de prendre fin en juin 2023, a été consacré à la numérisation (océrisation et annotation xml-TEI) du Dictionnaire universel, dans la version de 1701. Entrepris avec l'aide de Transkribus et de GROBID-Dictionnaries, ce projet a rempli une partie de ses objectifs, atteint d'autres, qui n'étaient pas prévus, mais aussi resté en-deçà des espoirs sur d'autres aspects. Parmi ces derniers on compte l'identification, la désambiguïsation et la gestion des entités nommées. En lien avec la thématique du séminaire, après une présentation générale du projet, il sera question des difficultés posées par les EN de type "lieu", et plus largement par la définition du domaine "géographie" tel que couvert par le DU de Basnage de Beauval.
