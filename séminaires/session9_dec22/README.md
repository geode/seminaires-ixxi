# Session 9 - Systèmes d’information géographique, Géomatique et visualisation des données.

Séance du 8 décembre 2022, 14h – 16h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne


## Programme


### Harvesting geospatial information from natural language texts

Xuke Hu (German Aerospace Center)


[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session9_dec22/seminaire-session9-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session9_dec22/seminaire-session9-presentation1.mp4 "Enregistrement présentation 1")

A vast amount of geospatial information exists in natural language texts (e.g., social media posts, website texts, and historical archives) in the form of toponyms, place names, and location descriptions. Extracting geographic information from texts is named geoparsing, which is beneficial not only for scientific studies, such as sociolinguistics and spatial humanities but can also contribute to various practical applications, such as disaster management, urban planning, and disease surveillance. 
In the presentation, I will share our latest findings in the two sub-tasks of geoparsing: toponym recognition and toponym resolution. Specifically, I will introduce our proposed approaches for the two sub-tasks and compare them with numerous existing ones based on many datasets.

**Related publications**

[1] Hu, X., Al-Olimat, H.S., Kersten, J., Wiegmann, M., Klan, F., Sun, Y. and Fan, H., 2022. GazPNE: annotation-free deep learning for place name extraction from microblogs leveraging gazetteer and synthetic data by rules. International Journal of Geographical Information Science, 36(2), pp.310-337. 

[2] Hu, X., Zhou, Z., Sun, Y., Kersten, J., Klan, F., Fan, H. and Wiegmann, M., 2022. GazPNE2: A general place name extractor for microblogs fusing gazetteers and pretrained transformer models. IEEE Internet of Things Journal.

[3] Hu, X., Zhou, Z., Li, H., Hu, Y., Gu, F., Kersten, J., Fan, H. and Klan, F., 2022. Location reference recognition from texts: A survey and comparison. arXiv preprint arXiv:2207.01683.

[4] Hu, X., Sun, Y., Kersten, J., Zhou, Z., Klan, F. and Fan, H., 2022. How can voting mechanisms improve the robustness and generalizability of toponym disambiguation? arXiv preprint arXiv:2209.08286.


### Crowdsourcing content for cultural geo-analytics: The case of Wikipedia

Andrea Ballatore (King's College London)


[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session9_dec22/seminaire-session9-presentation2.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session9_dec22/seminaire-session9-presentation2.mp4 "Enregistrement présentation 2")


Cultural geo-analytics is an emergent area that studies the geographical dimension of the production and consumption of cultural objects. User-generated content (UGC) provides valuable data to study a variety of cultural phenomena from a geographical angle, such as the construction and preservation of heritage. In this talk, selecting Greater London as a case study, I will discuss our work on how Wikipedia content evolves over time and space, analysing articles about geographical areas, points of interest, and geo-located events. Zooming out from London, I will then outline the many opportunities and challenges of using this kind of data sources to study the cultural sector.
