# Session 16 - Linguistique des discours, traitement automatique des langues.

Séance du 28 septembre 2023, 14h – 15h30
~~Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 185~~ et en ligne (lien zoom envoyé par mail après inscription).
Changement de lieu : INSA Lyon, Bâtiment Ada Lovelace, 3ème étage, salle 501.3.01.



## Programme


### Space Time Narratives: Understanding imprecise space and time in narratives through qualitative representations, reasoning, and visualisation.

Ian Gregory (Lancaster University) and members of the [Space Time Narratives project](https://spacetimenarratives.github.io)


[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session16_sep23/seminaire-session16.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session16_sep23/seminaire-session16.mp4 "Enregistrement présentation")


Previous approaches to understanding geographies in textual sources tend to focus on geoparsing to automatically identify place names and allocate them to coordinates. Such methods are highly quantitative and are limited to named places for which coordinates can be found, and have little concept of time. Yet, as narratives of journeys make abundantly clear, human experiences of geography are often subjective and more suited to qualitative representation. In these cases, "geography" is not limited to named places; rather, it incorporates the vague, imprecise, and ambiguous, with references to, for example, "the camp", or "the hills in the distance", and includes the relative locations using terms such as "near to", "on the left", "north of" or "a few hours’ journey from".