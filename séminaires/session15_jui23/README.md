# Session 15 - Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.

~~Séance du 22 juin 2023, 15h – 17h~~
~~Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne~~



## Programme


### Encyclopedias and Classification Issues when Researching the History of Knowledge

Peter Logan (Temple University)

Encyclopedias of the eighteenth and nineteenth centuries reproduce the knowledge of their time and place, as we know, but they do more than that. Comprehensive encyclopedias, like the Encyclopédie and Encyclopedia Britannica, in fact reproduce a specific kind of knowledge, that is, knowledge as defined by social elites rather than folk or tribal forms of knowledge. Along with it, they reproduce the structure in which the many branches of this knowledge were arranged. When identified and examined, the kind and structure of such encyclopedias can reveal new insights into the history of knowledge if we look at what counts (or does not count) as knowledge and examine how the constitution of this knowledge changes in time and space.

These basic questions are at the heart of the Nineteenth-Century Knowledge Project, which is dedicated to analyzing historical editions of Britannica. In this talk, I explain how we created digital editions of four Britannica editions from 1790-1911. But I focus primarily on the theoretical problem that we encountered when developing a system of classification for the 113,000 entries in these editions. Rather than relying solely on NLP, we elected to use controlled vocabularies. Such vocabularies are, themselves, products of their time and place. Like encyclopedias, they also represent knowledge as a structure, and they are no more or less objective in their treatment of categories than other historical documents. So how does one successfully index historical encyclopedias without erasing the historical structures of knowledge that they represent? 

