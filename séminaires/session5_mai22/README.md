# Session 5 - Linguistique des discours, traitement automatique des langues.

Séance du 5 mai 2022, 14h – 16h
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 024 (rez-de-chaussée) et en ligne


## Programme


### Early modern information overload: the Encyclopédie as big data

Glenn Roe (Sorbonne University)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session5_mai22/seminaire-session5-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session5_mai22/seminaire-session5-presentation1.mp4 "Enregistrement présentation 1")

[Supports de présentation](./Slides_presentation_01_Roe.pdf)

The exponential increase in the scale of humanities datasets is altering the relationship of scholars to the objects of their research. These ‘big data’ approaches—from data mining to distant reading—promise new insights into our shared cultural record in ways that would have previously been impossible. But, these same methods also risk disconnecting scholars from the raw materials of their research as individual texts are subsumed into massive digital collections. One of the main challenges for the digital humanities is to develop scalable reading approaches—both distant and close—that allow scholars to move from macro- to micro-analyses and back. In this talk, I will outline some previous attempts at addressing this challenge using data mining and machine learning techniques to explore large-scale datasets drawn primarily from the French Enlightenment period, and in particular the great mid-century Encyclopédie of Diderot and d’Alembert. Preliminary analysis of these datasets demonstrates that the overwhelming sense of ‘information overload’ that characterises our modern condition is in fact much older. From the Renaissance onwards, print culture was shaped by new information technologies such as indexing, commonplacing, and encyclopaedism, developed in order to make sense of the growing textual record. Today, as we grapple with our own data deluge, these techniques can help put our current fascination with big data into perspective, ensuring that the inherent specificity of humanistic enquiry remains viable and vibrant at any scale.
