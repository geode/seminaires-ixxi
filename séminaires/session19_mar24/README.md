# Session 19 - Linguistique des discours, traitement automatique des langues.

Séance du 11 mars 2024, 14h – 15h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne


## Comment créer un graphe de connaissances géospatial à partir de texte ? Une approche et son application aux Instructions nautiques


[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session19_mar24/seminaire-session19.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session19_mar24/seminaire-session19.mp4 "Enregistrement présentation")


L'extraction automatique d'informations géographiques à partir de texte est essentielle pour exploiter l'ensemble des connaissances spatiales qui n'existent que sous cette forme non structurée. Les éléments clés sont les entités spatiales, leurs types et les relations spatiales entre elles. Structurées en graphe de connaissances géospatial, les connaissances spatiales ambiguës peuvent être désambiguïsées, ce qui facilite considérablement leur accessibilité et réutilisation. Je présenterai la méthodologie ATONTE, une approche pour la création de graphes de connaissances géospatiaux à partir de texte, de connaissances d'experts et de données géographiques de référence. Tout au long de la présentation je montrerai la manière dont j'ai appliqué la méthodologie ATONTE aux Instructions nautiques afin de créer un graphe de connaissances géospatial de leur contenu. Les Instructions nautiques sont une série d'ouvrages publiés par le Shom (le Service hydrographique et océanographique de la Marine) qui décrivent l'environnement maritime côtier et donnent des instructions de navigation côtière. La création d'un graphe de connaissances géospatial du contenu des Instructions nautiques offre de nombreuses possibilités pour moderniser leur production, entretien et utilisation.
 
**Helen Rawsthorne**, Ingénieure de recherche, Mines Saint-Étienne, une école de l'Institut Mines-Télécom.

