# Session 13 - Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.


Séance du 24 avril 2023, 14h – 16h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne

Inscription : [https://framaforms.org/inscription-seminaire-avril-2023-traitement-de-donnees-complexes-en-geographie-1680732100](https://framaforms.org/inscription-seminaire-avril-2023-traitement-de-donnees-complexes-en-geographie-1680732100)

## Programme



### Le Dictionnaire universel de Trévoux (1704, 1771) 

Marie Leca-Tsiomis (CSLF, Univ. Paris Nanterre)

[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session13_avr23/seminaire-session13-presentation1.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session13_avr23/seminaire-session13-presentation1.mp4 "Enregistrement présentation")


Le Dictionnaire universel de Trévoux (1704, 1771) : son histoire montre comment, né d’une « charlatanerie », cet ouvrage devint le plus important dictionnaire français de la première moitié du 18e siècle, sur la critique duquel s’édifia une bonne partie de l’Encyclopédie.