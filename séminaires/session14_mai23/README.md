# Session 14 - Linguistique des discours, traitement automatique des langues.


Séance du 11 mai 2023, 14h – 16h
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne


## Programme



###  Repérage et annotation étendue des entités nominales de lieux

Mauro Gaio (Université de Pau)

[<img src="https://geode.liris.cnrs.fr/seminaires-ixxi/session14_mai23/seminaire-session14-presentation1.png" width="650" />](https://geode.liris.cnrs.fr/seminaires-ixxi/session14_mai23/seminaire-session14-presentation1.mp4 "Enregistrement présentation")


Le traitement des entités nommées (EN) est devenu incontournable en Traitement Automatique des Langues. Apparue dans les années 90 lors des conférences MUC (Message Understanding Conferences), les tâches comme la reconnaissance des EN et de leur catégorisation en par exemple : date, nom de personne, nom de lieu, organisation, etc. sont très fréquemment présentent dans diverses problématiques participant de l’analyse des contenus textuels.

Fort de ce succès, le traitement des EN s’oriente désormais vers de nouvelles perspectives avec, entre autres,  l'annotation étendue de ces unités et leur désambiguïsation. Ces défis rendent encore plus complexe la convergence vers une définition unique des EN,  d’autant que du point de vue linguistique  la question du statut théorique de celles-ci est encore aujourd'hui discutée.

Dans cette intervention on s’intéressera à deux aspects des EN de la catégorie nom des lieux. Le premier portera sur les deux formes dont un lieu peut être avoir exprimé dans un texte. Le deuxième portera sur l'une de ces formes, la forme nominale, dont la présence dans certains textes peut être majoritaire. Nous regarderons ensuite la chaîne de traitement permettant le repérage puis l'annotation étendue de ces entités nominales de lieux.

Ces travaux, combinant approche symbolique et approche par apprentissage profond, rendent également compte des possibilités d'une telle combinaison permettant, construction automatique du corpus d'apprentissage, repérage générale et annotation fine.

