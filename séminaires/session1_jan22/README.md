# Session 1 - Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.

Séance du 24 janvier 2022, 14h – 16h  
~~Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 (1er étage)~~ En ligne  

## Programme

1. Introduction à la conférence 
2. Présentation de la thématique 1 : Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIe s. et du XIXe s. 
3. Communications présentées :

### Les articles de géographie dans le Dictionnaire Universel de Trévoux et l’Encyclopédie de Diderot et d’Alembert 
D. Vigier (U. Lyon 2/ICAR), L. Moncla (INSA/LIRIS), I. Lefort (U. Lyon 2/EVS), K. McDonough (A. Turing Institute), T. Joliveau (U. J. Monnet/EVS)

On s’attachera d’abord à présenter les conditions d'élaboration de cette communication et son positionnement dans le cadre du projet collectif de GÉODE. On abordera ensuite les principaux résultats stabilisés à ce jour par cette recherche traitant de deux œuvres majeures de diffusion des savoirs au XVIIIe siècle. A la présentation du corpus échantillonné utilisé, qui réunit cent articles communs aux nomenclatures des deux œuvres et traitant de la France, on soulignera les caractéristiques discursives les plus saillantes de ce corpus qui ont été isolées pour chacun des deux dictionnaires, puis comparées entre elles et avec celles observables dans plusieurs dictionnaires plus anciens qui furent souvent utilisés comme sources par les auteurs.

[Supports de présentation](./Slides_presentation_01_GEODE.pdf)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session1_jan22/seminaire-session1-presentation1.jpg" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session1_jan22/seminaire-session1-presentation1.mp4 "Enregistrement présentation 1")



### Géographes et savoirs géographiques en Europe entre 1750 et 1850 : régimes de scientificité, production et légitimation scientifique.

Laura Péaud (U. Grenoble Alpes/PACTE)

Entre les années 1750 et 1850, les savoirs géographiques se transforment dans leurs modes de production et, plus encore, dans leurs régimes de légitimation et d’institutionnalisation. En effet, alors qu’au mitan du 18ème siècle, ils sont produits par des individus aux statuts divers (voyageurs, cosmographes, géographes de cabinet, cartographes) et indépendamment de toute institution, dans le courant du 19ème siècle les savoirs géographiques s’institutionnalisent et les discours, cartes, textes ou autres, se normalisent à l’échelle nationale et européenne. Les sociétés de géographie créées à partir des années 1820 traduisent ainsi ce processus de mise en ordre des savoirs géographiques produits jusqu’alors de façon dispersée. En envisageant les cas de la France, de la Prusse et de la Grande-Bretagne, cette intervention montrera dans quelle mesure l’institutionnalisation des modes de production des savoirs géographiques entre 1750 et 1850 répond à la volonté de légitimation et de visibilisation scientifique de la géographie. Dans le même temps, ce processus s’inscrit dans des logiques concomitantes, en particulier la nationalisation progressive des institutions et des savoirs géographiques.  

[Supports de présentation](./Slides_presentation_02_PEAUD.pdf)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session1_jan22/seminaire-session1-presentation2.jpg" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session1_jan22/seminaire-session1-presentation2.mp4 "Enregistrement présentation 2")
