# Session 6 - Linguistique des discours, traitement automatique des langues.

Séance du 26 septembre 2022, 14h30 – 16h30
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne



## Programme



### Ajout d'un étage d'analyse syntaxique à un corpus POS-tagué et lemmatisé de français classique.

Benoît Crabbé (Université Paris Cité / LLF) et Achille Falaise (CNRS / LLF)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session6_sep22/seminaire-session6-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session6_sep22/seminaire-session6-presentation1.mp4 "Enregistrement présentation 1")

[Supports de présentation](./Slides_presentation_01_LLF.pdf)


Le projet Presto (2013-2017) a permis la réalisation d'une chaîne de traitement pour l'analyse en parties du discours et la lemmatisation de textes depuis le français pré-classique jusqu’au français contemporain (du XVIe au XXIe siècles), pour divers types d'écrits. Nous présenterons ici des travaux menés dans le cadre du projet Géode (2020-2024), visant à améliorer cette chaîne, et à y ajouter un étage d'informations syntaxiques (dépendances), visant en particulier l'Encyclopédie de Diderot et d'Alembert (1751-1772).

Pour cela, nous utilisons un parseur neuronal, avec un modèle de langue du français moderne préexistant. Nous présenterons dans un premier temps son fonctionnement, et nous détaillerons les interfaces de cet analyseur avec les grands modèles de langue actuels. Nous présenterons ensuite comment nous avons intégré ce parser avec les ressources et la chaîne de traitement héritées de Presto. Nous terminerons par un aperçu de la plateforme qui nous permettra de faire corriger un échantillon de l'analyse obtenue par des annotateurs, afin d'évaluer sa qualité.


### De l’importance des données: datasets et outils pour le traitement automatique du Français d’Ancien Régime.

Simon Gabay (Université de Genève)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session6_sep22/seminaire-session6-presentation2.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session6_sep22/seminaire-session6-presentation2.mp4 "Enregistrement présentation 2")

[Supports de présentation](./Slides_presentation_02_Gabay.pdf)


Avec l’abandon progressif de l’apprentissage machine à base de règles, le traitement automatique des langues se retrouve confronté à un besoin de données sans précédent. Si pour les états de langues contemporains le problème reste marginal, il devient crucial pour des états plus anciens. Nous nous proposons donc de revenir sur les solutions développées dans le cadre du projet FreEM, de l’acquisition des données brutes jusqu’à l’annotation fine de corpora. Seront notamment passés en revue: l’analyse de mise en page, la reconnaissance optique de caractères, les modèles de langue, la lemmatisation, le POS-tagging, la normalisation linguistique et la reconnaissance d’entités nommées. Nous insisterons sur l’importance des collaborations interdisciplinaires, notamment entre philologues et TAListes pour la réussite des expériences.


