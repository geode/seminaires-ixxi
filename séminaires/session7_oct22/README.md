# Session 7 - Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.

Séance du 20 octobre 2022, 10h – 12h
Ecole Normale Supérieure de Lyon, bâtiment Recherche (D4), salle D4 179 et en ligne




## Programme


### Définir la Géographie : objectif scientifique dans l’Encyclopédie méthodique

Martine Groult (CNRS)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session7_oct22/seminaire-session7-presentation1.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session7_oct22/seminaire-session7-presentation1.mp4 "Enregistrement présentation 1")


L’Encyclopédie méthodique peut être considérée comme le lieu de la définition de la Géographie. Publiée de 1782 à 1832, elle consacre 14 volumes (11 de Discours et 3 de Cartes) à la Géographie et ce, de 1782 à 1827. La Géographie est ainsi la seule discipline à traverser toute la publication de l’Encyclopédie méthodique avec régularité. Après un bref regard sur l’origine du mot, à partir des recherches sur l’Histoire du Vocabulaire Scientifique avec Jacques Roger, nous aborderons les deux aspects de la Géographie dans l’Encyclopédie que sont : la déconsidération et les articles scientifiques, notamment Figure de la Terre de d’Alembert. Ce qui nous amènera aux présentations de la Géographie dans les Prospectus de Panckoucke puis par les directeurs des Dictionnaires. Le détachement de certaines disciplines, comme l’Histoire et la Politique, et la coopération avec d’autres comme les mathématiques, la physique etc., constituent le point fondamental pour cerner les fluctuations de la définition de la géographie aux XVIIIe et XIXe siècles. Enfin, la définition de la Géographie est aussi une question philosophique dont Kant s’est emparé, ce qui constitue également le signe que c’est une discipline essentielle pour l’époque.



### Géographie en territoire encyclopédique : La Grande Encyclopédie  (1885-1902)

Christine Jacquet-Pfau (Cergy Paris Université)

[<img src="https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session7_oct22/seminaire-session7-presentation2.png" width="650" />](https://projet.liris.cnrs.fr/geode/seminaires-ixxi/session7_oct22/seminaire-session7-presentation2.mp4 "Enregistrement présentation 1")


La Grande Encyclopédie. Inventaire raisonné des sciences, des lettres et des arts (1885-1902), publiée « par une société de savants et de gens de lettres » offre, en cette fin du XIXe siècle scientiste, un panorama précis des sciences dont plusieurs sont en cours de constitution en tant que disciplines scientifiques et objets d’enseignement. Nous examinerons dans cette présentation la place qu’y occupe la géographie et sa mise en discours encyclopédique, à travers différents parcours. Seront ainsi abordées la conception de la géographie elle-même, les classifications descriptives dont elle fait l’objet, les caractéristiques lexicographiques et terminologiques mises en place. Nous présenterons également la distribution des notices entre les différents auteurs qui ont contribué à leur rédaction. Enfin, nous sélectionnerons quelques entrées pour mettre en valeur la nouvelle dimension accordée à cette science et le rôle de transmission d’un savoir de « haute vulgarisation » (« Préface ») des trente-et-un volumes de LGE.