# Extraction, traitement et visualisation de données complexes en géographie (XVIIIe s. - XIXe s.) 

Cycle de conférences organisées à l’[Ecole Normale Supérieure de Lyon](http://www.ens-lyon.fr) et financées par l’Institut Rhônalpin des systèmes Complexes ([IXXI](http://www.ixxi.fr/)).

La géographie du XVIIIe s. reste relativement mal connue dans le champ de l’histoire de la géographie. Or elle constitue un maillon essentiel pour comprendre épistémologiquement ce qui se joue durant ce siècle. La géographie des Lumières se présente en effet en tension entre héritages de savoirs et de pratiques du début de la période moderne, et projet d’organisation et de disciplinarisation qui se dessine dès la fin du XVIIIe s.
Le cycle de conférences que nous proposons ici pour la période 2021-2023 se fixe un double but :

-  ressaisir cette tension à travers le prisme de trois grandes encyclopédies françaises parues entre 1751 et 1902 ;
-  Intéresser un réseau national et international à la mise au point d’une interface de navigation interactive à même d’articuler a) une visualisation cartographique des lieux et des populations évoquées dans ces encyclopédies, b) une visualisation des discours les plus significatifs sur le plan textométrique tenus à propos d’eux, c) la possibilité à tout moment de contextualiser ces données grâce à des informations accessibles relatives à l’épistémologie et à l’histoire de la géographie entre le XVIIIes. et le XIXe s.

***
### Thèmes

Trois grandes thématiques structurent ce cycle et alterneront dans le temps :
1. Histoire et épistémologie de la géographie au prisme des encyclopédies françaises du XVIIIes. et du XIXe s.
2. Linguistique des discours, traitement automatique des langues.
3. Systèmes d’information géographique, Géomatique et visualisation des données 

***
### Programme des sessions

| Séance            | Thème                     | Détails et inscription | Invités        |
| ----------------- | ------------------------- | --------------- | ------------ |
| 11 mars 2024 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session19_mar24) | Helen Rawsthorne (Mines Saint-Etienne)|
| 18 janvier 2024 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session18_jan24) | [Nathalie Rousseau](https://lettres.sorbonne-universite.fr/personnes/nathalie-rousseau) (Sorbonne Université) et [Anaïs Chambat](https://achambat.github.io) (CY Cergy Paris Université) |
| 4 décembre 2023 | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session17_dec23) | [Ioana Galleron](http://www.univ-paris3.fr/mme-galleron-ioana-468922.kjsp) (Sorbonne Nouvelle) |
| 28 septembre 2023 | TAL et SIG | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session16_sep23) | [Ian Gregory](https://www.lancaster.ac.uk/staff/gregoryi/) (Lancaster University) and members of the [Space Time Narratives project](https://spacetimenarratives.github.io) |
| ~~22 juin 2023~~ | ~~Histoire et épistémologie~~ | [Annulé](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session15_jui23) | ~~[Peter Logan](https://liberalarts.temple.edu/academics/faculty/logan-peter-m) (Temple University)~~ |
| 11 mai 2023 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session14_mai23) | [Mauro Gaio](https://lma-umr5142.univ-pau.fr/fr/organisation/membres/cv_-mgaio-fr.html) (Univeristé de Pau) |
| 24 avril 2023 | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session13_avr23) | [Marie Leca-Tsiomis](http://cths.fr/hi/personne.php?id=5928) (CSLF, Univ. Paris Nanterre) |
| 30 mars 2023 | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session12_mar23) | [Gilles Bertrand](https://luhcie.univ-grenoble-alpes.fr/membres/gilles-bertrand-2/) (Université de Grenoble)  |
| 20 février 2023 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session11_fev23) | [Julien Perret](https://www.umr-lastig.fr/julien-perret/) (IGN) & [Joseph Chazalon](https://www.lrde.epita.fr/wiki/User:Chazalon) (EPITA) |
| 24 janvier 2023 | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session10_jan23) | [Nicolas Verdier](https://geographie-cites.cnrs.fr/membres/nicolas-verdier/) (CNRS, EHESS) |
| 8 décembre 2022 | Systèmes d'information géographique | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session9_dec22) | [Xuke Hu](https://www.dlr.de/dw/en/desktopdefault.aspx/tabid-12207/21398_read-37880/sortby-lastname/) (German Aerospace Center) & [Andrea Ballatore](https://aballatore.space) (King's College London) |
| 24 novembre 2022 | Systèmes d'information géographique | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session8_nov22) | [Rainer Simon](https://rsimon.github.io/index.html) (Austrian Institute of Technology) & [Valeria Vitale](https://www.sheffield.ac.uk/dhi/about-dhi/people/valeria-vitale) (University of Sheffield)|
| 20 octobre 2022 | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session7_oct22) | [Martine Groult](https://fr.wikipedia.org/wiki/Martine_Groult) (CNRS) & [Christine Jacquet-Pfau](https://www.researchgate.net/profile/Christine-Jacquet-Pfau) (Cergy Paris Université) |
| 26 septembre 2022 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session6_sep22) | [Simon Gabay](https://www.unige.ch/lettres/humanites-numeriques/equipe/collaborateurs/dr-simon-gabay) (Université de Genève) & [Benoît Crabbé](http://www.llf.cnrs.fr/fr/Gens/Crabbe) (LLF) et [Achille Falaise](https://pro.aiakide.net) (LLF) |
| 5 mai 2022 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session5_mai22) | [Glenn Roe](https://cellf.cnrs.fr/membre/glenn-roe/) (Sorbonne Université) |
| 8 avril 2022 | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session4_avr22) | [Isabelle Turcan](https://turcanisabelle.wordpress.com/) (U. de Lorraine) & [Henri Desbois](https://www.parisnanterre.fr/m-henri-desbois) (U. de Paris Nanterre) |
| 15 mars 2022 | Systèmes d'information géographique | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session3_mar22) | [Alexis Litvine](https://www.hist.cam.ac.uk/people/dr-alexis-litvine) (U. of Cambridge/CAMPOP) & [Thierry Joliveau](https://umr5600.cnrs.fr/fr/lequipe/name/thierry-joliveau/) (U. Saint-Etienne/EVS)|
| 14 février 2022 | Linguistique des discours et TAL | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session2_fev22) | [Antoine Doucet](https://l3i.univ-larochelle.fr/Doucet-Antoine-Pr) (U. La Rochelle/L3i) & [Ludovic Moncla](https://ludovicmoncla.github.io) (INSA Lyon/LIRIS) |
| 24 janvier 2022  | Histoire et épistémologie | [Slides et vidéos](https://gitlab.liris.cnrs.fr/geode/seminaires-ixxi/-/tree/master/séminaires/session1_jan22)| [Laura Péaud](https://www.pacte-grenoble.fr/membres/laura-peaud) (U. Grenoble Alpes/PACTE) & [Denis Vigier](http://www.icar.cnrs.fr/membre/dvigier/) (U. Lyon 2/ICAR)|






***
### Equipe organisatrice

Coordinateur du projet :  [Denis Vigier](http://www.icar.cnrs.fr/membre/dvigier/) (ICAR, Univ. Lyon 2)  
Partenaires : [Thierry Joliveau](https://mondegeonumerique.wordpress.com/) (EVS, Univ. St-Etienne), [Katherine McDonough](https://www.turing.ac.uk/people/researchers/katherine-mcdonough) (The Alan Turing Institute), [Ludovic Moncla](https://ludovicmoncla.github.io/) (LIRIS, INSA Lyon), [Isabelle Lefort](https://www.iufrance.fr/les-membres-de-liuf/membre/1484-isabelle-lefort.html) (EVS, Univ. Lyon 2),  [Denis REYNAUD](http://ihrim.ens-lyon.fr/auteur/reynaud-denis) (IRHIM, Univ. Lyon 2)

